Object-based audio production dataset "The Turning Forest"
==========================================================

This dataset contains an object-based audio scene packaged as a project for the digital audio workstation Reaper http://www.reaper.fm

It is available through the DOI 10.17866/rd.salford.9766943 https://doi.org/10.17866/rd.salford.9766943 .

To use this dataset, the VISR Production Suite, a set of free and open-source VST3 plugins, must be downloaded and installed from 
https://s3a-spatialaudio.org/plugins .

The content has been created as part of the EPRSC programme grant EP/L000539/1 (EPSRC) "S3A: Future spatial audio for an immersive listening experience at home".

When using this work, please cite it as

S3A project, “The Turning Forest - An object-based production dataset", 2019, DOI: 10.17866/rd.salford.9766943

and/or

Costantini, G., Franck, A., Pike, C., Francombe, J., Woodcock, J., Hughes, R., Coleman, P., Whitmore, E., and Fazi, F. M., 
“A Dataset of High-Quality Object-Based Productions,” in AES 147th Convention, New York City, NY, USA, 2019, Engineering Brief.

The most current version of the dataset is also available as a public git repository:
https://gitlab.eps.surrey.ac.uk/s3a-public/s3a-scene-the-turning-forest .

Note: git LFS (Large File Storage) must be installed on the host computer to correctly download the audio content.
See https://help.github.com/en/articles/installing-git-large-file-storage for guidance.

License
-------

This work is licensed under the Creative Commons 
Attribution-NonCommercial-NoDerivatives 4.0 International License.
To view a copy of this license, visit 
http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter
to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
